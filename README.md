﻿# ANE20 - Analoge Elektronica 2 #

Deze repository is bedoeld voor studenten en docenten van de opleiding Elektrotechniek van de Hogeschool Rotterdam en wordt gebruikt om aanvullend studiemateriaal voor de cursus "ANE20 - Analoge Elektronica 2" te verspreiden.

Alle informatie is te vinden op de [Wiki](https://bitbucket.org/HR_ELEKTRO/ane20/wiki/).